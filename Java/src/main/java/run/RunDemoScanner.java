package run;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.cli.ParseException;

import tspiper.lib.FieldContainer;
import tspiper.lib.ParsingError;
import tspiper.lib.Record;
import tspiper.mysql.RecordScannerSQL;
import tspiper.utils.StopWatch;

public class RunDemoScanner {


	public static void main(String[] args) throws IOException, ParseException, ParsingError, SQLException {

		// Parsing command line arguments
	    CommandLineDTO cmd = new CommandLineDTO(args);
        
        FieldContainer key = new FieldContainer(cmd.kfields);
        StopWatch sw = new StopWatch();
        RecordScannerSQL scanner = new RecordScannerSQL(cmd.config, cmd.start, cmd.end, key, cmd.maxLength);
        
        long counter = 0;
        double volume = 0.0;
        for (Record record = scanner.next(); record != null; record = scanner.next()) {
            volume += record.getValueDouble("volume");
            ++counter;
        }
        sw.setEndTime();
        
        System.out.println("Average volume: " + (volume/counter));
        System.out.println("Record count  : " + counter);
        System.out.println("Execution time: " + 1.0 * sw.getDifference()/1000_000_000L + "s");        
		
	}

}
