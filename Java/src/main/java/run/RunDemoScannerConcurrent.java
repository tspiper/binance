package run;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.cli.ParseException;

import tspiper.lib.FieldContainer;
import tspiper.lib.ParsingError;
import tspiper.lib.Record;
import tspiper.mysql.RecordScannerSQL;
import tspiper.typecompr.Type;
import tspiper.utils.StopWatch;

public class RunDemoScannerConcurrent {

    static class RequestThread extends Thread
    {
        private final String ticker;
        private final FieldContainer key;
        private final String config;
        private final String start;
        private final String end;
        private final Long maxLength;
      
        RequestThread(String ticker, String config, String start, String end, Long maxLength) throws IOException, SQLException, ParsingError
        {
            this.ticker = ticker;
            key = new FieldContainer("type", Type.STRING, "USDM_FUT");
            key.addString("ticker", ticker);
            this.config = config;
            this.start = start;
            this.end = end;
            this.maxLength = maxLength;
        }
      
        public void run()
        {            
            RecordScannerSQL scanner;
            try {
                scanner = new RecordScannerSQL(config, start, end, key, maxLength);
            } catch (IOException | SQLException | ParsingError e) {                
                e.printStackTrace();
                return;
            }            
            double volume = 0.0;
            int counter = 0;
            for (Record record = scanner.next(); record != null; record = scanner.next()) {
                volume += record.getValueDouble("volume");
                ++counter;
            }
            
            System.out.println(String.format("Average volume [%s]: %f", this.ticker, (volume/counter)));
            System.out.println("Record count  : " + counter);
            
        }
    }
    
	public static void main(String[] args) throws IOException, ParseException, ParsingError, SQLException, InterruptedException {

		// Parsing command line arguments
	    
	    CommandLineDTO cmd = new CommandLineDTO(args);
	    String tickers[] = {"BNBUSDT", "BTCUSDT", "ETHUSDT", "ADAUSDT"};
	    ArrayList<Thread> threads = new ArrayList<>();
        
        StopWatch sw = new StopWatch();
	    for (String ticker: tickers) {
	        threads.add(new RequestThread(ticker, cmd.config, cmd.start, cmd.end, cmd.maxLength));
	    }
	            

        for (Thread thread : threads) {
            thread.start();            
        }
        
        
        for (Thread thread : threads) {
            thread.join();            
        }
        
        sw.setEndTime();
        
        System.out.println("Execution time: " + 1.0 * sw.getDifference()/1000_000_000L + "s");        
		
	}

}
