package run;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import tspiper.utils.RelaxedParser;

public class CommandLineDTO {

    private static Class<?> clazz = java.lang.invoke.MethodHandles.lookup().lookupClass();
    private static String canonicalName = clazz.getCanonicalName();
    
    public String config = null;
    public String kfields = null;
    public String start = null;
    public String end = null;
    public Long maxLength = null;
    
    public CommandLineDTO(String[] args) throws ParseException {
        // Parsing command line arguments

        Options options = new Options();

        options.addRequiredOption("c", "config", /* hasArg */true, "config file (yaml)");
        options.addOption("k", "kfields", /* hasArg */true, "json from key fields");
        options.addRequiredOption("s", "start", /* hasArg */true, "startTime");
        options.addRequiredOption("e", "end", /* hasArg */true, "endTime");
        options.addOption("m", "maxLength", /* hasArg */true, "maximum length");

        CommandLineParser parser = new RelaxedParser();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException exp) {
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(canonicalName, options);
            System.exit(1);
        }

        config =  (String) cmd.getParsedOptionValue("config");
        kfields = (String) cmd.getParsedOptionValue("kfields");
        start = (String) cmd.getParsedOptionValue("start");
        end = (String) cmd.getParsedOptionValue("end");
        maxLength = Long.valueOf(cmd.getOptionValue("maxLength", "1000000")); 
    }
    
}
