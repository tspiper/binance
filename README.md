

# ![image info](./images/logo_b.png) TSPiper 

*TSPiper* is a general purpose time series data storage framework. It is designed to store and process large quantities of time series data in the most efficient way. It has the following features:
 - Horizontal scaling; 
 - Multithreading capabilities;
 - Could be based on SQL or non-SQL platform;
 - Supports PySpark;
 - Depending on data, TSPiper has 7% to 30% better compression ratio than gzip, mostly due to proprietary compression algorithm for normally distributed integers;
 - Allows efficient remote data retrieval since data is transferred in compressed form;
 - Allows on-server parallel data processing (up to 3 million records/sec on each node) for time consuming calculations;
 - Could be deployed on a cloud or on-site.
 
# Binance data access

*Binance data access* utilises remote data capability of the *TSPiper* framework. It provides access to Binance [Trades](https://binance-docs.github.io/apidocs/futures/en/#aggregate-trade-streams) (the Aggregate Trade Streams push market trade information that is aggregated for a single taker order every 100 milliseconds.) and [Level2 snapshots](https://binance-docs.github.io/apidocs/futures/en/#partial-book-depth-streams) (top 10 levels of bids and asks, updated every 100 milliseconds) for futures. The data could be accessed using Python, R and Java. The examples are provided below. All examples require the following files:
 - `tspiper_read.jar` - the framework itself with all required libraries stored inside the jar file (fat jar);
 - `trade_biance.yml` - the configuration file with the information on the database and table, allowing access to trades;
 - `quote_biance.yml` - the configuration file with the information on the database and table, allowing access to Level2.

The read only version of the framework is provided which allows users to read and process data, but not to compress and store data.

The data is take from Binance [USD-M Futures Websocket datastreams](https://binance-docs.github.io/apidocs/futures/en/#websocket-market-streams) 

To access data using remote version of the framework, the user will need to install Java Development Kit, for example, [OpenJDK 11](https://openjdk.java.net/projects/jdk/11/).

## Python

The example is given for Jupyter notebook (`binance-demo.ipynb`) and Python3 (`binance-demo.py`).
In both cases Python access is based on [pyjnius](https://pyjnius.readthedocs.io/en/stable/) library which should be installed prior to use: 

```
pip install pyjnius
```
*pyjnius* is Python wrapper around Java. 

To successfully run provided examples, the user might need to modify *lib_path* to *jar* and configuration *yaml* files. The default value of this variable should work if the project is pulled with *git* and the structure of folders is kept unchanged:

```
lib_path='../lib'
```
The example builds a scanner (iterator) class based on
 - *startTime* - UTC start time of the data
 - *endTime* - UTC end time 
 - *type* -  the srouce of data. For USD-M Futures, the type is "USDM-FUT"
 - *ticker* - the code for crypto ("BNB", "BTC" or "ETH") 

The Python example calculates the average trade size in given period. The Jupyter example has additionally an example cell how to convert data into Pandas dataframe.
![](./images/df.png)

**Note:** The framework is very powerful and it is easy to underestimate the amount of memory required to process the request. The count example does not allocate  memory on user's workstation and any number of records could be processed. The dataframe example allocates memory for each record. To prevent user's workstation to run out of memory, the framework limits number of received records by 1 million independently of other parameters. This limitation could be changed by adding extra parameter in the scanner class constructor.  For example, 

```
scanner = RecordScanner(lib_path + '/trade_binance.yml', startTime, endTime, key, 10000000)
```
will create a scanner with the potential to access 10 million records.

## R

The R example utilises [rJava](https://cran.r-project.org/web/packages/rJava/rJava.pdf) library to communicate with Java. It consists of the following files:
 - `TSPiperLib.R` - low level example how to access Java classes
 - `TSPiperDemo.R` - containing example of the trades and quotes requests
 - `TSPiperDemoQuotes.R` - more complicated example which shows executions on top of Level2 data
 
 ![](./images/executions.png)
 
Examples create dataframes from the first 10000 records of the request. The code is very similar to the Python example, although *rJava* is not as efficient as *pyjnius*.
 
## Java
 
 Java is the fastest way to access data. 
 
 `RunDemoScanner` provides the example of using *TSPiper* framework to calculate the average trade size. The task is reduced to a simple *for* loop:  
 
 ```
    for (Record record = scanner.next(); record != null; record = scanner.next()) {
        volume += record.getValueDouble("volume");
        ++counter;
    }
    sw.setEndTime();

 ```
By default, without any data specification, scanner initialises the base class *Record*. If configured, the framework could provide specialised  *QuoteBinance* or *TradeBinance* classes where user can implement convenient methods accessing the data. To access this feature, the initialised class should be specified in the configuration *yaml* file:

```
record: "data.TradeBinance"
```

Java demo has extended conig files with correct initialisation of specified class. Then the example above could be rewritten as

```
    for (Record record = scanner.next(); record != null; record = scanner.next()) {
        TradeBinance trade = (TradeBinance) record;
        volume += trade.getVolume();
        ++counter;
    }
```
which provides higher performance comparing to access data using field name.

 `RunDemoScannerConcurrent` provides the example of using *TSPiper* framework to access data concurrently. Such an approach  will be effective if the data is processed near or on the server with data.

## C++

The C++ example explicitly utilises Java Native Interface (JNI). The header file *jni.h* is part of Java Development Kit and provides the bridge between Java and C++. JNI defines two key data structures, *JavaVM* and *JNIEnv*. Both of these are essentially pointers to pointers to function tables. 

The *JavaVM* consists of invocation interface functions, which allow you to create and destroy a Java Virtual Machine. In theory you can have multiple JavaVMs per process (see JVM class in *jvm.h*).

The *JNIEnv* provides most of the JNI functions. Your native functions all receive a *JNIEnv* as the first argument. The *JNIEnv* is used for thread-local storage. For this reason, you cannot share a *JNIEnv* between threads. 

The example consists of the following files:
 - `compile.sh` - a simple compilation line, which creates *trade.exe* executable (C++ 17)
 - `jclasses.h` - C++ wrappers around concrete Java classes which return C++ objects and primitives 
 - `jwrapper.h` - the base class for all the wrappers 
 - `trades.cpp` - the example of extraction of trades for a given period

To perform a Java call, two JNI calls are required: the first one to find the pointer to the method using the function signture and the second is the call of the method itself. The function pointer stays the same for all runs. It is conveniently cached with the use of static member which is the singleton and executed only once. Below you have the example: 

```
std::string Record::toString() const {
    static jmethodID s_method = getMethodID("toString", "()Ljava/lang/String;");
    return jobjectToString(m_env->CallObjectMethod(m_obj, s_method));
}
```

To avoid unnecessary memory allocations, the record scanner wrapper class has two implementations of method *next*, which provides consecutive stream of data. First of all, it is method `Record next()` which creates new instance of `Record` and secondly it is method `void nextInit(Record& record)` which reinitialises existing object `Record` with new data. 
 
The C++ example calculates the average trade size and is more efficient in terms of performance comparing to the Python example.
 