import time
from datetime import datetime
import jnius_config

lib_path='../lib'
jnius_config.set_classpath('.', lib_path + '/*')
from jnius import autoclass

RecordScanner = autoclass('tspiper.mysql.RecordScannerSQL')
FieldContainer = autoclass('tspiper.lib.FieldContainer')

startTime = "2022-02-02T00:00:00Z"
endTime   = "2022-02-03T00:00:00Z"
key = FieldContainer()
key.addString("type", "USDM_FUT")
key.addString("ticker", "BNBUSDT")


print("Reading data...")
scanner = RecordScanner(lib_path + '/trade_binance.yml', startTime, endTime, key)

record = scanner.next()
recordFirst = record


start = time.perf_counter()
total = 0.0
count = 0
while record != None :
    total += record.getValue("volume")    
    count = count + 1
    
    record = scanner.next()
    
finish = time.perf_counter()
print("Total records:", count)
print("Average volume:", total/count)
print("Execution time: ", finish - start, " seconds")
