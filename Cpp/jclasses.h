#ifndef JCLASSES__H
#define JCLASSES__H

#include<string>
#include "jwrapper.h"

class Record : public JWrapper<Record> {

public:
	Record(JNIEnv* env, jobject obj) : JWrapper(env, "tspiper/lib/Record") {
        m_obj = obj;
    }

	std::string toString() const {
		static jmethodID s_method = getMethodID("toString", "()Ljava/lang/String;");
		return jobjectToString(m_env->CallObjectMethod(m_obj, s_method));
	}

	std::string getValueString(const char* name) const {
        static jmethodID s_method = getMethodID("getValueString", "(Ljava/lang/String;)Ljava/lang/String;");
        return jobjectToString(m_env->CallObjectMethod(m_obj, s_method, toJString(name)));
	}

	double getValueDouble(const char* name) const {
        static jmethodID s_method = getMethodID("getValueDouble", "(Ljava/lang/String;)D");
        return m_env->CallDoubleMethod(m_obj, s_method, toJString(name));
	}

	long getValueLong(const char* name) const {
        static jmethodID s_method = getMethodID("getValueLong", "(Ljava/lang/String;)J");
        return m_env->CallLongMethod(m_obj, s_method, toJString(name));
	}

};

class FieldContainer : public JWrapper<FieldContainer> {
public:
    FieldContainer(JNIEnv* env) : JWrapper(env, "tspiper/lib/FieldContainer") {
        static jmethodID construct = getMethodID("<init>", "()V");
        m_obj = (env)->NewObject(m_clazz, construct);
    }

    void addString(const char* field, const char* val) const {
        static jmethodID s_method = getMethodID("addString", "(Ljava/lang/String;Ljava/lang/String;)Z");
        m_env->CallObjectMethod(m_obj, s_method, m_env->NewStringUTF(field), m_env->NewStringUTF(val));
    }

};

class RecordScannerSQL : public JWrapper<RecordScannerSQL> {

public:
	RecordScannerSQL(JNIEnv* env, const char* config, const char* date1, const char* date2, FieldContainer key) : JWrapper(env, "tspiper/mysql/RecordScannerSQL") {
        static jmethodID construct = getMethodID("<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltspiper/lib/FieldContainer;)V");
        m_obj = (env)->NewObject(m_clazz, construct, m_env->NewStringUTF(config), m_env->NewStringUTF(date1), m_env->NewStringUTF(date2), key.getJObject());
    }

    void addString(const char* field, const char* val) const {
        static jmethodID s_method = getMethodID("addString", "(Ljava/lang/String;Ljava/lang/String;)Z");
        m_env->CallObjectMethod(m_obj, s_method, m_env->NewStringUTF(field), m_env->NewStringUTF(val));
    }

    Record next() const {
        static jmethodID s_method = getMethodID("next", "()Ltspiper/lib/Record;");
        jobject jrecord = m_env->CallObjectMethod(m_obj, s_method);
        return Record(m_env, jrecord);
    }

    void nextInit(Record& record) const {
        static jmethodID s_method = getMethodID("next", "()Ltspiper/lib/Record;");
        jobject jrecord = m_env->CallObjectMethod(m_obj, s_method);
        record.setJObject(jrecord);
    }

};

#endif
