#ifndef JWRAPPER__H
#define JWRAPPER__H

#include<jni.h>

/**
 *  The class is utilising the Curiously Recursive Template Pattern because class type m_clazz is unique for each wrapped Java class.
 */
template<class T>
class JWrapper {
public:

	JWrapper(JNIEnv* env, const char* clazz) {
    	m_env = env;
        m_clazz = init(clazz);
	}

    jobject getJObject() const {
    	return m_obj;
    }

    void setJObject(jobject obj) {
    	m_obj = obj;
    }

    bool isNull() const {
    	return m_obj == NULL;
    }

protected:
    jclass init(const char* name) {
       static jclass result = m_env->FindClass(name);

       if (NULL == result) {
           std::string error = std::string("Cannot find class ") + name;
           std::cout <<  error << std::endl;
    	   throw error;
       }

       return result;
    }

    jmethodID getMethodID(const char* method, const char* arg) const {
    	jmethodID methodID = m_env->GetMethodID(m_clazz, method, arg);
        if (NULL == methodID) {
            std::string error = std::string("Cannot find method ") + method + " " + arg;
            std::cout <<  error << std::endl;
     	   throw error;
        }
        return methodID;
    }

    std::string jobjectToString(jobject jstr) const {
	    const char* str = m_env->GetStringUTFChars((jstring) jstr, NULL);
	    return std::string(str);
    }

    jstring toJString(const char* str) const {
    	return m_env->NewStringUTF(str);
    }


protected:
	JNIEnv* m_env;  // Pointer to methods of Java machine
	jclass m_clazz; // Java class
	jobject m_obj;  // Java object

};

#endif
