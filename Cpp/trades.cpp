#include<iostream>
#include <chrono>

#include "jvm.h"
#include "jclasses.h"


int main(int argc, char** argv) {
    
	auto jar = (char *) "-Djava.class.path=/home/danyliv/git/binance/lib/tspiper_read.jar";
    JVM jvm = JVM(jar);
    JNIEnv *env = jvm.getEnv();

    FieldContainer key = FieldContainer(env);
    key.addString("type", "USDM_FUT");
    key.addString("ticker", "BNBUSDT");


    auto config = "/home/danyliv/git/binance/lib/trade_binance.yml";
    auto time1 = "2022-02-02T00:00:00Z";
    auto time2 = "2022-02-03T00:00:00Z";

    auto start = std::chrono::high_resolution_clock::now();
    RecordScannerSQL scanner = RecordScannerSQL(env, config, time1, time2, key);
    Record record = scanner.next();

    int count = 0;
    double total = 0.0;
    while(!record.isNull()) {

       /**
        * Uncomment to print records

        std::cout << record.toString() << std::endl;

        */

        total += record.getValueDouble("volume");
        count = count + 1;

        scanner.nextInit(record);
    }

    std::cout << " Total records: " << count << std::endl;
    std::cout << "Average volume: " << total/count << std::endl;

    auto finish = std::chrono::high_resolution_clock::now();
    auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(finish-start);
    std::cout << "Execution time: " << milliseconds.count()/1000. << " seconds" << std::endl;

}
