#ifndef JVM__H
#define JVM__H

#include<jni.h>

class JVM {
    JavaVM *vm;
    JNIEnv *env;

public:
    JVM(char* arg) {
		JavaVMOption* options = new JavaVMOption[1];
		options[0].optionString = arg;
		JavaVMInitArgs vm_args; /* JDK/JRE  VM initialization arguments */
		vm_args.version = JNI_VERSION_1_8;
		vm_args.nOptions = 1;
		vm_args.options = options;
		vm_args.ignoreUnrecognized = false;

		/* load and initialize a Java VM, return a JNI interface
		 * pointer in env */
		JNI_CreateJavaVM(&vm, (void**)&env, &vm_args);
    }

    JNIEnv* getEnv() {
    	return env;
    }

    ~JVM() {
    	vm->DestroyJavaVM();
    }

};

#endif
